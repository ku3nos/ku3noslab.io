---
title: "The open house day"
date: 2020-01-31
draft: false
tags: ["Ku3noS"]
---

#### The open house day

![](/theopenday/tagderoffenentuer.jpeg)

Now the time has finally come, the open day is here. We present our project on the second floor of the HTL-Villach - Tschinowitscherweg 5. We have the privilege to introduce the project and thus our school to potential new students. In this way students can get an insight in our work and what the school offer.
We are ready, so we hope we will see you!

#### Whats next

The last step concerning our project is the persentation on 7th of February. There we have to present our project to our supervisors. This is finally the end of this project, however we immediately start with a new project for Preperation to the diploma thesis.
We are back soon!



