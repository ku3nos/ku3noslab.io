---
title: "PatrioT – Diploma thesis"
date: 2020-06-29
draft: false
tags: ["diploma-thesis"]
---

#### Beginning
We are back with a new project, our diploma thesis. We are going to cooperate with the UNI in Klagenfurt, where we got the possibility to work together with our supervisor Daniel Neuhold. The fundamental idea is, to locate the so called TAG’s on Millimetres exactly. Our part of this project is, to create some different showcases to test this technology in combination with smart devices. This includes the part of a normal Smart Home or some companies.

![](/Diplomathesis/logosmall.png)


#### What’s next
So far we already started with the technical work on our project and we also absolved the Kickoff-Meeting. The plan will be to work over the summer holidays, so that we could finish this part in September. The next months from September till April, we have to write down all the needed documentations to complete our diploma thesis.



