
---
title: "Preparation Project"
date: 2020-02-27
draft: false
tags: ["diploma-preparation"]
---

#### Overview
In the summer semester of this school year, we have the possibility to create a project, which support us to carry out the diploma thesis. We are very happy, that we have the chance to make our diploma thesis at the University in Klagenfurt. Together with our supervisor we created a very cool preparation project.

#### Basics of the project
Now we want to give you a short overview about our first ideas:
In our project we want to virtualize a 2D building plan. Out of this plan a 3D model should be generated. With the help of new ultra wide band sensors created from our supervisor we will try to mark the measured point in the model. Additionally, we will implement more smart devices to try to communicate with these devices.
The use of this project is
To learn how to deal with the different parts of this new technology for instance the exact localization of the small chip with help of the access points
To get the knowledge about new technologies
In best case to reuse some small parts for our diploma thesis

#### What' next?
These are the first drafts and collections of ideas that we can implement. We will soon be able to give you a better insight and more precise information about our work and the first results.




