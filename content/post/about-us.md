---
title: "About Us"
date: 2019-12-25
draft: false
tags: ["AboutUs"]
---

#### Who are we?
We want to welcome you to our first blog. First, we want to introduce us. We are three students from the higher technical colleague Villach, namely Maximilian Kleinsasser, Thomas Urschitz and Stefan Urschitz. Together, we are attending the network engineering (cyber security) department. We are currently attending the fourth form in this school.
#### What do we want?
The aim of this writing is to inform interested people about our doing, our skills and our projects. Moreover, we want to show our path to the diploma thesis. This is a good tool for us to provide a steadily progress of our projects and we want to give interested people the possibility to follow us. 
#### How can you contact us?
If you have questions, suggestions or improvements feel free to contact one of us to our email – addresses. 

- maximilian.kleinsasser@edu.htl-villach.at
- thomas.urschitz@edu.htl-villach.at
- stefan.urschitz@edu.htl-villach.at

We are thankful, that you take time for us!

![](/aboutus/openday2020.jpeg)
