---
title: "Finished our preparation project"
date: 2020-06-05 
draft: false
tags: ["diploma-preparation"]
---

#### Summary
We worked about 250 hours on our preparation project, where we created a 3D model from a 2D floor plan. We located the Tag (the small chip you carry with you) and displayed them into the 3D building. The following picture gives an overview about our topology:

![](/finished-diplomapreparationproject/topology.png)


Now we want show you some nice results of this very nice project. Through an image recognition process, which was adapted by us we were able to prepare the floor plan, so that we just need to give this plan a height. The next two figures will show first of all the plan and then resulting 3D model.

![](/finished-diplomapreparationproject/plan.png)

![](/finished-diplomapreparationproject/3Dmodel.png)


After the 3D model was created, it was time to place the Access Points on this building. To display the model we used Three.js, which is a very nice tool running in the Browser. The last step was to write some functions which are able to place the Access Points and set the Tag's position reading the data from the Firebase Realtime Database. This last picture shows how these step will look, when it is finished.

![](/finished-diplomapreparationproject/markedAP.png)



#### Resumee
We completely reached our goals. We learned about the technology we will work with in our diploma thesis, we could adjust to our supervisor`s workstyle, which is very well-organized and we could collect new Know-How in terms of management, planning and cooperation with companies and people working in economy.


#### What's next
The next step will be to prepare our diploma thesis and start in the summer holiday with the practical work. Special thanks to our supervisor Daniel Neuhold, who organized and helped us to get this incredible chance to work on this project. It gives us so much Know-How and it is very nice to work in such a motivated and well-organized team, thank you.
We hope you got a little insight into our work on the preparation project. If there are any questions, please feel free to contact us and otherwise we will back soon with an update concerning our diploma thesis.



