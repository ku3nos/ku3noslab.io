---
title: "Project-Week"
date: 2020-01-29
draft: false
tags: ["Ku3noS"]
---

#### Projectweek
![](/projectweek/imageproject.jpeg) 

In this week everything was devoted to the open house day at the HTL-Villach. We worked and improved our projekt as well as we incorporate improvements concerning the experience we collected at our presentation on 13.01.2020. For a quick insight view our last blog entry.
Our project is ready, so we hope we will see you on Friday 31th of January 2020.

#### Safe the date

On Friday, 31th of January, we are going to present our project in the cybersecurity room located on the second floor of HTL-Villach.
