---
title: "Our First Project"
date: 2019-12-25T04:38:46-09:00
draft: false
tags: ["Ku3noS"]
---

#### General Information
![](/ourfirstproject/topology.PNG)

This post introduces our current project, Ku3noS. We started this project in September 2019 and it should be finished at the begin of February. Our main target is to present our work at the open day of our school at Jan, 31th 2020.
#### Explanation
For us, this project is like a simple experiment. We have displayed a structure of our project above. Now we want to talk shortly about the different parts. 
- The image detection part aims to get the number and the colour of the card. We worked here with image comparison and opencv to provide, that the image detection works. After that, the result is transmitted over FTP to the Website.

![](/ourfirstproject/imagecompare.PNG)
- The Website displays the card game “uno”. The computer is playing against the detected cards. Before players are getting to this site, they have to type in their credentials.
- Our Kali client is checking the password security with hashcat. We have the possibility to point out the importance of security to young people with our next and last part, the statistics.
- The statistics website is currently under development. We want to show some facts and our statistics in form of commonly used passwords or other things like that. 
#### Conclusion
We are currently working at our project. After finishing our different parts, we are looking forward for an internal presentation. 


