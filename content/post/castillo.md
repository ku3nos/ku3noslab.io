---
title: "Castillo"
date: 2020-04-12T04:38:46-09:00
draft: false
tags: ["diploma-preparation"]
---

#### Special Thanks
Before we begin we want to thank three people, that we are at this state of the project:
- https://www.aau.at/team/neuhold-daniel/ ,our supervisor for providing us nice possibilities and helping us everytime.
- https://gitlab.com/chaosjolo ,for helping us with css and html.
- https://twitter.com/m4r10k ,for helping us with container and the appropriate setup. 

#### Current State
As you know from our first post, we want to virtualise floor plans. We used image recognition libraries like opencv and tensorflow to process the image. After that we used blender as render engine to create a new 3d object. Everything is automatically - have a look.
![](/castillo/model.jpg)

Of course, we wanted to have the object on a Webpage. We used three.js to display our blender object in the webbrowser. Further, we gave the user the possibility to draw windows, doors and lines on a floor plan. The modelling can be started also over the webside.

#### What's next?
Next week we have our first milestone for this project. After that continue our work with three.js.
 


